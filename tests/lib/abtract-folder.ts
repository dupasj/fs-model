import AbtractFolderInterface from "../../src/abstract/folder";
import testRootImplmentation from "./root";
import Path from "path";
import fs from "fs";

const testAbstractFolderImplementation = (AbtractFolder: {new (): AbtractFolderInterface,fromPath(path: string): AbtractFolderInterface}) => {
    testRootImplmentation(AbtractFolder);

    describe("Test Abstract Folder implementation",() => {
        describe("Test loader implementation",() => {
            it("Can get the size of a file from the loader method",async () => {
                const root = AbtractFolder.fromPath(__filename);

                 expect(await root.loadSize()).toBeGreaterThan(0);
            });
            it("Can get the size of a file from the direct method",async () => {
                const root = AbtractFolder.fromPath(__filename);

                const stat = await root.loadStatistics();

                expect(await root.getSize()).toStrictEqual(stat.size);
            });
        });
        describe("Test stats implementation",() => {
            it("The stats value is null by default",() => {
                const root = new AbtractFolder();
                expect(root.statistics).toBeNull();
            });
            describe("Test stats exist",() => {
                it("Return true when the abstract directory have the stats",() => {
                    const root = new AbtractFolder();

                    root.statistics = new fs.Stats();

                    expect(root.hasStatistics()).toBeTruthy();
                });
                it("Return false when the abstract directory does not have the stats",() => {
                    const root = new AbtractFolder();

                    expect(root.hasStatistics()).toBeFalsy();
                });
            });
            describe("Test stats remove",() => {
                it("Set the value to null when we remove the value",() => {
                    const root = new AbtractFolder();

                    root.statistics = new fs.Stats();
                    root.removeStatistics();

                    expect(root.statistics).toBeNull();
                });
                it("Set the parents Abstract directory stats to null",() => {
                    const root = new AbtractFolder();
                    const parent = new AbtractFolder();

                    root.statistics = new fs.Stats();
                    parent.statistics = new fs.Stats();

                    root.directory = parent;
                    root.removeStatistics();

                    expect(parent.statistics).toBeNull();
                });
                it("Return the Abstract Parent when we remove the stats values",() => {
                    const root = new AbtractFolder();

                    expect(root.removeStatistics()).toStrictEqual(root);
                });
            });
        });

        describe("Test exist implementation",() => {
            it("The exist value is null by default",() => {
                const root = new AbtractFolder();
                expect(root.exits).toBeNull();
            });
            describe("Test exist setter",() => {
                it("Can set the exist information",() => {
                    const root = new AbtractFolder();

                    root.setIsExists(true);

                    expect(root.exits).toStrictEqual(true);
                });
                it("Return the abstract directory when we set the exist value",() => {
                    const root = new AbtractFolder();

                    expect(root.setIsExists(true)).toStrictEqual(root);
                });
                it("Set to true the directory directory directory when when the directory exist",() => {
                    const root = new AbtractFolder();
                    const parent = new AbtractFolder();

                    root.directory = parent;

                    root.setIsExists(true);

                    expect(parent.exits).toStrictEqual(true);
                });
            });
            describe("Test exist value exist",() => {
                it("Return true when the abstract directory have the exist value",() => {
                    const root = new AbtractFolder();

                    root.exits = true;

                    expect(root.hasExists()).toBeTruthy();
                });
                it("Return false when the abstract directory does not have the exist value",() => {
                    const root = new AbtractFolder();

                    expect(root.hasExists()).toBeFalsy();
                });
            });
            describe("Test exist getter",() => {
                it("Can get the exist value",() => {
                    const root = new AbtractFolder();

                    root.exits = true;

                    expect(root.isExists()).toStrictEqual(true);
                });
                it("Throw an error when the abtract directory does not have the exist value",() => {
                    const root = new AbtractFolder();

                    expect(() => {
                        root.isExists();
                    }).toThrowError();
                });
            });
            describe("Test exist loader",() => {
                it("Return true when the file exist",async () => {
                    const root = AbtractFolder.fromPath(__filename);

                    expect(await root.checkForExists()).toBeTruthy();
                    expect(root.isExists()).toBeTruthy();
                });
                it("Return true when the directory exist",async () => {
                    const root = AbtractFolder.fromPath(__dirname);

                    expect(await root.checkForExists()).toBeTruthy();
                    expect(root.isExists()).toBeTruthy();
                });
                it("Return false when the directory do not exist",async () => {
                    const root = AbtractFolder.fromPath(Path.join(__dirname,"not-existing"));

                    expect(await root.checkForExists()).toBeFalsy();
                    expect(root.isExists()).toBeFalsy();
                });
                it("Use the exist setter when we fetch if the value exist",async () => {
                    const root = AbtractFolder.fromPath(__filename);

                    const spy = jest.spyOn(AbtractFolder.prototype, 'setIsExists');

                    await root.checkForExists();

                    expect(spy).toBeCalledWith(true);
                });
            });
        });
        describe("Test path implementation",() => {
            describe("Test directory exist",() => {
                it("Return true when the abstract directory can get the path value",() => {
                    const root = new AbtractFolder();
                    const parent = new AbtractFolder();

                    parent.basename = "test";
                    root.basename = "test";

                    root.directory = parent;

                    expect(root.canGetPath()).toBeTruthy();
                });
                it("Return false when the directory could not have the path value",() => {
                    const root = new AbtractFolder();
                    const parent = new AbtractFolder();

                    root.basename = "test";
                    root.directory = parent;

                    expect(root.canGetPath()).toBeFalsy();
                });
                it("Return false when the item could not have the path value",() => {
                    const root = new AbtractFolder();
                    const parent = new AbtractFolder();

                    parent.basename = "test";

                    root.directory = parent;

                    expect(root.canGetPath()).toBeFalsy();
                });
            });
            it("Test path getter",() => {
                const root = new AbtractFolder();
                const parent = new AbtractFolder();

                parent.basename = "test";
                root.basename = "test";

                root.directory = parent;

                expect(root.getPath()).toStrictEqual(Path.normalize("test/test"));
            });
        });
        describe("Test directory implementation",() => {
            it("The directory value is null by default",() => {
                const root = new AbtractFolder();
                expect(root.directory).toBeNull();
            });

            describe("Test directory setter",() => {
                it("Can set the directory",() => {
                    const root = new AbtractFolder();
                    const parent = new AbtractFolder();

                    root.setDirectory(parent);

                    expect(root.directory).toStrictEqual(parent);
                });
                it("Return the abstract directory when we set the directory value",() => {
                    const root = new AbtractFolder();
                    const parent = new AbtractFolder();

                    expect(root.setDirectory(parent)).toStrictEqual(root);
                });
            });
            describe("Test directory exist",() => {
                it("Return true when the abstract directory have a directory value",() => {
                    const root = new AbtractFolder();

                    root.directory = new AbtractFolder();

                    expect(root.hasDirectory()).toBeTruthy();
                });
                it("Return true when the abstract directory does not have a directory value",() => {
                    const root = new AbtractFolder();

                    expect(root.hasDirectory()).toBeFalsy();
                });
            });
            describe("Test directory getter",() => {
                it("Can get the directory value",() => {
                    const root = new AbtractFolder();
                    const parent = new AbtractFolder();

                    root.directory = parent;

                    expect(root.getDirectory()).toStrictEqual(parent);
                });
                it("Throw an error when the abtract directory does not have directory value",() => {
                    const root = new AbtractFolder();

                    expect(() => {
                        root.getDirectory();
                    }).toThrowError();
                });
            });
        });
    });
};

export default testAbstractFolderImplementation;
