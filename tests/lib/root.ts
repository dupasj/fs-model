import RootInterface from "../../src/root";

const testRootImplmentation = (Root: {new (): RootInterface}) => {
    describe("Test Root Class",() => {
        describe("Test basename implementation",() => {
            it("The root basename is null by default",() => {
                const root = new Root();
                expect(root.basename).toBeNull();
            });

            describe("Test basename setter",() => {
                it("Can set the basename",() => {
                    const root = new Root();

                    root.setBasename("test");

                    expect(root.basename).toStrictEqual("test");
                });
                it("Return the object when when set the basename",() => {
                    const root = new Root();

                    root.setBasename("test");

                    expect(root.setBasename("test")).toStrictEqual(root);
                });
            });
            describe("Test basename exist",() => {
                it("Return true when the root have a basename",() => {
                    const root = new Root();

                    root.basename = "test";

                    expect(root.hasBasename()).toBeTruthy();
                });
                it("Return true when the root does not have a basename",() => {
                    const root = new Root();

                    expect(root.hasBasename()).toBeFalsy();
                });
            });
            describe("Test basename getter",() => {
                it("Can get the root basename",() => {
                    const root = new Root();

                    root.basename = "test";

                    expect(root.getBasename()).toStrictEqual("test");
                });
                it("Throw an error when the root does not have basename",() => {
                    const root = new Root();

                    expect(() => {
                        root.getBasename();
                    }).toThrowError();
                });
            });
        });
    });
};

export default testRootImplmentation;