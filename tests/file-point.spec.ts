import "jest";
import testAbstractFolderImplementation from "./lib/abtract-folder";
import File from "../src/file";
import FilePoint from "../src/file-point";

describe("Test File Point implementation",() => {
    describe("Test File implementation",() => {
        it("The file instance value is null by default",() => {
            const point = new FilePoint();

            expect(point.file).toBeNull();
        });
        describe("Check setter",() => {
            it("Can set the file instance value",() => {
                const point = new FilePoint();
                const file = new File();

                point.setFile(file);

                expect(point.file).toStrictEqual(file);
            });
            it("Return the instance when we set the File instance",() => {
                const point = new FilePoint();
                const file = new File();

                expect(point.setFile(file)).toStrictEqual(point);
            });
        });
        describe("Check exist",() => {
            it("Return true when the File Point doesn't have file value",() => {
                const point = new FilePoint();
                const file = new File();

                point.file = file;

                expect(point.hasFile()).toBeTruthy();
            });
            it("Return false when the File Point doesn't have file value",() => {
                const point = new FilePoint();
                expect(point.hasFile()).toBeFalsy();
            });
        });
        describe("Check getter",() => {
            it("Can get the File instance value",() => {
                const point = new FilePoint();
                const file = new File();

                point.file = file;

                expect(point.getFile()).toStrictEqual(file);
            });
            it("Throw an error when the file point does not have File instance",() => {
                const point = new FilePoint();

                expect(() => {
                    point.getFile();
                }).toThrowError();
            });
        });
    });
    describe("Test position implementation",() => {
        it("The position value is null by default",() => {
            const point = new FilePoint();

            expect(point.position).toBeNull();
        });
        describe("Check setter",() => {
            it("Can set the position value",() => {
                const point = new FilePoint();

                point.setPosition(15);

                expect(point.position).toStrictEqual(15);
            });
            it("Return the instance when we set the position value",() => {
                const point = new FilePoint();

                expect(point.setPosition(15)).toStrictEqual(point);
            });
        });
        describe("Check exist",() => {
            it("Return true when the File Point have the position value",() => {
                const point = new FilePoint();

                point.position = 15;

                expect(point.hasPosition()).toBeTruthy();
            });
            it("Return false when the File Point doesn't have position value",() => {
                const point = new FilePoint();
                expect(point.hasPosition()).toBeFalsy();
            });
        });
        describe("Check getter",() => {
            it("Can get the position value",() => {
                const point = new FilePoint();
                const file = new File();

                point.position = 15;

                expect(point.getPosition()).toStrictEqual(15);
            });
            it("Throw an error when the file point does not have the position value",() => {
                const point = new FilePoint();

                expect(() => {
                    point.getPosition();
                }).toThrowError();
            });
        });
    });
});
