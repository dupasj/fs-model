import "jest";
import testAbstractFolderImplementation from "./lib/abtract-folder";
import File from "../src/file";
import { v4 as uuidv4 } from 'uuid';
import Folder from "../src/folder";
import * as Path from "path";

describe("Test File Implementation",() => {
    testAbstractFolderImplementation(File);

    describe("Test safe destroy implementation",() => {
        it("Can destroy the file",async () => {
            const file = File.fromPath(uuidv4()+".txt");
            await file.save("content file");

            const spy = jest.spyOn(File.prototype,"destroy");

            await file.safeDestroy();

            expect(await file.checkForExists(false)).toBeFalsy();
            expect(spy).toBeCalledTimes(1);

            spy.mockRestore();
        });
        it("Do not try to remove the file if the file do not exist",async () => {
            const file = File.fromPath(uuidv4()+".txt");

            const spy = jest.spyOn(file,"destroy");

            await file.safeDestroy();

            expect(spy).not.toBeCalled();

            spy.mockRestore();
        });
        it("Check if the exist or not",async () => {
            const file = File.fromPath(uuidv4()+".txt");

            const spy = jest.spyOn(File.prototype,"checkForExists");

            await file.safeDestroy(false);

            expect(spy).toBeCalledWith(false);

            spy.mockRestore();
        });
    });
    describe("Test destroy implementation",() => {
        it("Can destroy the file",async () => {
            const file = File.fromPath(uuidv4()+".txt");
            await file.save("content file");
            await file.destroy();

            expect(await file.checkForExists(false)).toBeFalsy();
        });
        it("Do not check if the file exist or not",async () => {
            const file = File.fromPath(uuidv4()+".txt");
            await file.save("content file");


            const spy = jest.spyOn(file,"checkForExists");

            await file.destroy();

            expect(spy).not.toBeCalled();

            spy.mockRestore();
        });
        it("Set the file to inexist when the file is removed",async () => {
            const file = File.fromPath(uuidv4()+".txt");
            await file.save("content file");

            const spy = jest.spyOn(file,"setIsExists");

            await file.destroy();

            expect(spy).toBeCalledWith(false);

            spy.mockRestore();
        });
    });
    describe("Test save implementation",() => {
        const folder = Folder.fromPath(uuidv4());

        beforeAll(async () => {
            await folder.save();
        });
        afterAll(async () => {
            await folder.safeDestroy();
        });

        it("Can create the file",async () => {
            const file = File.fromPath(uuidv4()+".txt");
            file.setDirectory(folder);

            await file.save("content file");

            expect(await file.checkForExists(false)).toBeTruthy();
        });
        it("Set the content when we give the file content",async () => {
            const content = uuidv4();

            const file = File.fromPath(uuidv4()+".txt");
            file.setDirectory(folder);
            file.content = Buffer.from(uuidv4());

            await file.save(content);

            expect(file.content).toStrictEqual(Buffer.from(content));
        });
        it("Set the value to true when we create the file",async () => {
            const file = File.fromPath(uuidv4()+".txt");
            file.setDirectory(folder);

            const spy = jest.spyOn(file,"setIsExists");

            await file.save("test");

            expect(spy).toBeCalledWith(true);

            spy.mockRestore();
        });
        it("Save the parent directory",async () => {
            const file = File.fromPath("test/"+uuidv4()+".txt");
            file.setDirectory(folder);

            const spy_directory_save = jest.spyOn(folder,"save");

            spy_directory_save.mockRestore();

            await file.save("test");

            expect(await file.getDirectory().checkForExists(false)).toBeTruthy();
            expect(await file.checkForExists(false)).toBeTruthy();
        });
        it("Test the static call",async () => {
            const file = await File.save(Path.join(folder.getPath(),"test/"+uuidv4()+".txt"),"content");

            expect(await file.checkForExists(false)).toBeTruthy();
        });
    });
});
