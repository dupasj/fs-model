# FS Model

This module allows you to manage the file with class who offer a easy scaling.

## More Scalable

FS Model will allows you to manage our file more faster and easer. For exemple, you can extends the File class to edit our JSON file such as like: 
```
    import File from "@dupasj/fs-model/file";

    class Json extends File{
        value: any;

        setContent(content: string){
            super.setContent(content);

            this.value = JSON.parse(content);
            
            return this;
        }

        getValue(){
            return this.value;
        }
        hasValue(){
            return typeof this.value !== "undefined";        
        }
        setValue(value: any){
            super.setContent(JSON.stringify(value));
            this.value = value;

            return this;
        }
    }
```

## Issues

This is my first npm repository. 
Indeed, feel free to add an issue on my [gitlab repository](https://gitlab.com/dupasj/fs-model/-/issues), I will be happy to answers you.
