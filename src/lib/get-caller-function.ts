const getCallerFunction = (file: string) => {
    const original_funct = Error.prepareStackTrace;

    let caller;
    try {
        const err = new Error();

        Error.prepareStackTrace = function (err, stack) { return stack; };

        // @ts-ignore
        while (err.stack.length > 1) {
            // @ts-ignore
            const caller = err.stack.shift().getFileName();

            if(file === caller){
                Error.prepareStackTrace = original_funct;

                // @ts-ignore
                return err.stack.shift().getFileName();
            }
        }
    } catch (e) {}

    Error.prepareStackTrace = original_funct;

    throw new Error();
};

export default getCallerFunction;
