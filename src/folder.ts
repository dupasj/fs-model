import fs from "fs";
import * as Path from "path";
import AbtractFolder from "./abstract/folder";
import File from "./file";
import getCallerFunction from "./lib/get-caller-function";

class Folder extends AbtractFolder{
    childs: AbtractFolder[] = [];
    childs_loaded: boolean = false;

    async destroy(){
        return this.remove();
    }

    async safeDestroy(){
       await this.truncate();

       return this.destroy();
    }

    async safeRemove(){
        const exist = await this.checkForExists();
        if (!exist){
            return this;
        }

        for(const child of this.getChilds()){
            if (child.hasExists() && child.isExists()){
                return this;
            }
        }

        await Promise.all(this.getChilds().map(child => child.checkForExists()));

        for(const child of this.getChilds()){
            if (child.isExists()){
                return this;
            }
        }

        return this.destroy();
    }

    async remove(){
        return new Promise((resolve,reject) => {
           fs.rmdir(this.getPath(),(err) => {
               if (err){
                   reject(err);
               }else{
                   this.setIsExists(false);
                   resolve();
               }
           })
        });
    }

    async truncate(){
        await this.loadChilds();

        return Promise.all(this.getChilds().map(child => child.safeDestroy()));
    }

    mergeChild(child: AbtractFolder){
        if (!this.hasChild(child)){
            this.addChild(child);
        }

        return this;
    }

    addChild(child: AbtractFolder){
        if (this.hasChild(child)){
            throw new Error();
        }

        this.childs.push(child);



        child.setDirectory(this);

        return child;
    }

    getChilds(){
        return this.childs;
    }
    getChildsFolders(): Folder[] {
        // @ts-ignore
        return this.getChilds().filter(child => child instanceof Folder);
    }
    getChildsFiles(): File[] {
        // @ts-ignore
        return this.getChilds().filter(child => child instanceof File);
    }

    list(){
        return this.getChilds();
    }
    listFolders(): Folder[]{
        return this.getChildsFolders();
    }
    listFiles(): File[] {
        return this.getChildsFiles();
    }

    hasChild(child: AbtractFolder|string): boolean {
        if (child instanceof AbtractFolder){
            if (this.childs!.includes(child)){
                return true;
            }

            if (!this.canGetPath()){
                return false;
            }

            return this.hasChild(child.getPath());
        }

        const normalized = Path.normalize(child);
        if (normalized !== child){
            return this.hasChild(normalized);
        }

        return this.getChilds().filter(item => {
            return item.getPath() === Path.normalize(child);
        }).length > 0;
    }


    async loadFiles(){
        await this.loadChilds();

        return this.getChildsFiles();
    }
    async loadFolders(){
        await this.loadChilds();

        return this.getChildsFolders();
    }
    loadList(){
        return this.loadChilds();
    }
    loadChilds(){
        if (this.childs_loaded){
            return Promise.resolve(this.getChilds());
        }

        return new Promise((resolve,reject) => {
            fs.readdir(this.getPath(),async (err,result) => {
                if (err){
                    reject(err);
                }else{
                    this.setIsExists(true);

                    const folders: Folder[] = [];

                    for(const item of result){
                        const full_path = Path.join(this.getPath(),item);

                        folders.push(Folder.fromPath(full_path));
                    }

                    Promise.all(folders.map(folder => folder.loadStatistics())).catch(reject).then(() => {
                        for(const folder of folders){
                            folder.setIsExists(true);

                            if (folder.getStatistics().isDirectory()){
                                this.mergeChild(folder);
                            }else{
                                this.mergeChild(File.fromFolder(folder));
                            }
                        }

                        this.childs_loaded = true;
                        resolve(this.getChilds());
                    });
                }
            });
        });
    }

    mkdir(){
        return new Promise((resolve,reject) => {
            fs.mkdir(this.getPath(),{
                recursive: true
            },(err) => {
                if (err){
                    reject(err);
                }else{
                    this.setIsExists(true);
                    resolve(this);
                }
            })
        });
    }

    async save() {
        const exist = await this.checkForExists();
        if (!exist){
            return this.mkdir();
        }

        return this;
    }

    static fromPath(path: string): Folder {
        if (!Path.isAbsolute(path)){
            return this.fromPath(Path.join(Path.dirname(getCallerFunction(__filename)),path));
        }

        const result = new Folder();

        const dirname = Path.dirname(path);

        if (dirname.length > 0 && dirname !== path){
            result.setBasename(Path.basename(path));

            result.setDirectory(Folder.fromPath(dirname));
        }else{
            result.setBasename(dirname);
        }

        return result;
    }

    static listFolders(path: string){
        return this.fromPath(path).loadFolders();
    }

    static listFiles(path: string){
        return this.fromPath(path).loadFiles();
    }

    static list(path: string){
        return this.fromPath(path).loadList();
    }

    static remove(path: string){
        return this.fromPath(path).remove();
    }

    static safeRemove(path: string){
        return this.fromPath(path).safeRemove();
    }

    static truncate(path: string){
        return this.fromPath(path).truncate();
    }

    static destroy(path: string){
        return this.fromPath(path).destroy();
    }

    static safeDestroy(path: string){
        return this.fromPath(path).safeDestroy();
    }

    static mkdir(path: string){
        return this.fromPath(path).mkdir();
    }

    static save(path: string){
        return this.fromPath(path).save();
    }
    
}

AbtractFolder.prototype.setDirectory = function(parent: AbtractFolder){
    if (parent instanceof Folder && !parent.hasChild(this)){
        parent.mergeChild(this);

        return this;
    }

    this.directory = parent;

    return this;
};

export default Folder;
