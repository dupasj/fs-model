import File from "./file";
import FileSection from "./file-section";

class FilePoint{
    position: number|null = null;
    file: File|FileSection|null = null;
    file_sections: FileSection[] = [];

    mergeFileSection(file_section: FileSection){
        if (this.hasFileSection(file_section)){
            return this;
        }

        return this.addFileSection(file_section);
    }

    addFileSection(file_section: FileSection){
        if (this.hasFileSection(file_section)){
            throw new Error();
        }

        if (
            (!file_section.hasStart() || file_section.getStart() !== this)
            && (!file_section.hasEnd() || file_section.getEnd() !== this)
        ){
            throw new Error();
        }

        this.file_sections.push(file_section);

        return this;
    }

    hasFileSection(file_section: FileSection){
        return this.getFileSections().includes(file_section);
    }

    getFileSections(){
        return this.file_sections;
    }

    setFile(file: File|FileSection){
        if (file !== this.file){
            this.file = file;

            for(const file_section of this.getFileSections()){
                file_section.setFile(file);
            }
        }

        return this;
    }
    hasFile(){
        return this.file !== null;
    }
    getFile(){
        if (!this.hasFile()){
            throw new Error("You cannot get the file instance from the file point because the value is undefined");
        }

        return this.file!;
    }

    setPosition(position: number){
        this.position = position;

        return this;
    }
    hasPosition(){
        return this.position !== null;
    }
    getPosition(){
        if (!this.hasPosition()){
            throw new Error("You cannot get the file instance from the file point because the value is undefined");
        }

        return this.position!;
    }

    getLine(){
        const content = this.getFile().getContent().toString();

        return content.split("\n").length;
    }

    getColumn(){
        const content = this.getFile().getContent().toString();

        return content.slice(content.lastIndexOf("\n")).length;
    }


    static fromFile(file: File,position?: number) {
        const result = new FilePoint();
        result.setFile(file);

        if (typeof position !== "undefined"){
            result.setPosition(position);
        }

        return result;
    }
    static fromPosition(position: number,file?: File) {
        const result = new FilePoint();
        result.setPosition(position);

        if (typeof file !== "undefined"){
            result.setFile(file);
        }

        return result;
    }
}

export default FilePoint;
