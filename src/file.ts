import Folder from "./folder";
import fs from "fs";
import * as Path from "path";
import AbtractFolder from "./abstract/folder";
import getCallerFunction from "./lib/get-caller-function";
import FileSection from "./file-section";

class File extends AbtractFolder{
    content: Buffer|null = null;

    /**
     * @param folder The folder who will conveted
     * @description Convert an folder instance to a file instance
     * @return Return the File created from the Folder instance.
     */
    static fromFolder(folder: AbtractFolder): File {
        return Object.assign(new File(), folder);
    }

    /**
     * @param cache A boolean who specify if you can to use the cache.
     * @description Destroy the file if the file exist.
     */
    async safeDestroy(cache: boolean = true): Promise<this> {
        const exist = await this.checkForExists(cache);
        if (exist){
            return this.destroy();
        }

        return this;
    }

    /**
     * @description Remove the file
     */
    destroy(): Promise<this>{
        return new Promise<this>((resolve,reject) => {
            fs.unlink(this.getPath(),(err) => {
               if (err){
                   reject(err);
               } else{
                   this.setIsExists(false);
                   resolve(this);
               }
            });
        });
    }

    /**
     * @param content The new content of the file
     * @description Create the parent directory and save the file
     */
    async save(content?: string): Promise<this> {
        if (this.hasDirectory()){
            const parent = this.getDirectory();

            if (parent instanceof Folder){
                await parent.save();
            }
        }

        if (typeof content !== "undefined"){
            this.setContent(Buffer.from(content));
        }

        return new Promise<this>((resolve,reject) => {
           fs.writeFile(this.getPath(),this.getContent(),(err) => {
               if (err){
                   reject(err);
               }else{
                   this.setIsExists(true);
                   resolve(this);
               }
           })
        });
    }

    /**
     * @description Set the content of the instance. This will not affect our file system. If you want to set the content of the file, you can use save method.
     * @param content The new content of the instance
     */
    setContent(content: Buffer|string): this{
        if (typeof content === "string"){
            return this.setContent(Buffer.from(content));
        }

        this.content = content;

        return this;
    }

    /**
     * @return Return a boolean if the file instance have a content or not
     */
    hasContent(): boolean {
        return this.content !== null;
    }

    /**
     * @description Return the content of the File instance
     * @throws Throw an error if our the file instance do not have content
     */
    getContent(): Buffer {
        if (!this.hasContent()){
            throw new Error("You cannot get the content value because this value is empty.");
        }

        return this.content!;
    }

    /**
     * @description Load the content of the file from our File system.
     * @param cache A boolean who specify if you can to use the cache.
     */
    loadContent(cache: boolean = true): Promise<Buffer> {
        if (this.hasContent() && !cache){
            return Promise.resolve(this.getContent());
        }

        return new Promise<Buffer>((resolve,reject) => {
            fs.readFile(this.getPath(),(err,content) => {
                if(err){
                    reject(err);
                }else{
                    this.setIsExists(true);
                    this.setContent(content);
                    resolve(this.getContent());
                }
            });
        });
    }

    /**
     * @description Build a File instance from the path.
     * @param path The path of our file.
     */
    static fromPath(path: string): File {
        if (!Path.isAbsolute(path)){
            return this.fromPath(Path.join(Path.dirname(getCallerFunction(__filename)),path));
        }

        const result = new File();

        const basename = Path.basename(path);
        const dirname = Path.dirname(path);

        result.setBasename(basename);

        if (dirname.length > 0){
            result.setDirectory(Folder.fromPath(dirname));
        }

        return result;
    }

    /**
     * @param path The File where the file will be localed
     * @param content The content file
     */
    static save(path: string,content: string): Promise<File> {
        return this.fromPath(path).save(content);
    }

    static fromContent(content: string|Buffer){
        const result = new File();
        result.setContent(content);

        return result;
    }

    toSection(){
        const section = new FileSection();
        section.setStart(0);
        section.setEnd(this.getContent().length);
        section.setFile(this);

        return section;
    }
}

export default File;
