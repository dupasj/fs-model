import fs from "fs";
import Path from "path";
import Root from "../root";

abstract class AbtractFolder extends Root{
    directory: null|AbtractFolder = null;
    statistics: fs.Stats|null = null;
    exits: null|boolean = null;

    abstract safeDestroy(): Promise<any>
    abstract destroy(): Promise<any>

    /**
     * @param exists
     */
    setIsExists(exists: boolean){
        if (exists && this.hasDirectory()){
            this.getDirectory().setIsExists(true);
        }

        this.exits = exists;

        return this;
    }

    /**
     * @return boolean Return a boolean who idencate if the instance have the cache for the file or folder who exist or not.
     */
    hasExists(){
        return this.exits !== null;
    }

    checkForExists(cache: boolean = true): Promise<boolean> {
        if (this.hasExists() && !cache){
            return Promise.resolve(this.isExists());
        }

        return new Promise<boolean>((resolve) => {
            fs.access(this.getPath(), fs.constants.F_OK, (err) => {
                this.setIsExists(err === null);

                resolve(this.isExists());
            });
        });
    }

    /**
     * @throws Throw an error if the value is undefined.
     * @return boolean Return a boolean who idencate if the file or folder exist or not.
     */
    isExists(): boolean {
        if (!this.hasExists()){
            throw new Error("You cannot check if the value exist or not because the check has not been save to this instance.");
        }

        return this.exits!;
    }

    /**
     * @description Return the size of the file from the statics informations.
     * @return number
     */
    getSize(): number {
        return this.getStatistics().size;
    }

    /**
     * @param cache Set to the value to false if you want to force to load the statistics from the file or the directory.
     * @description Same as getSize but this method will load our statistics first.
     * @return Promise<number>
     */
    async loadSize(cache: boolean = false): Promise<number> {
        return (await this.loadStatistics(cache)).size;
    }

    /**
     * @return boolean Return a boolean if the instance can get the absolute path.
     */
    canGetPath(): boolean {
        return this.hasBasename() && (!this.hasDirectory() || this.getDirectory().canGetPath());
    }

    /**
     * @return string Return the normalised absolute path of the instance.
     */
    getPath(): string {
        if (this.hasDirectory()){
            return Path.normalize(Path.join(this.getDirectory().getPath(),this.getBasename()));
        }

        return Path.normalize(this.getBasename());
    }

    /**
     * @param statistics The statistics informations who will be setted.
     * @description Set the statistics informations of the instances.
     * @return this
     */
    setStatistics(statistics: fs.Stats): this {
        this.statistics = statistics;

        return this;
    }

    /**
     * @description Check if the instance have the statistics informations or not.
     * @return boolean
     */
    hasStatistics(): boolean {
        return this.statistics !== null;
    }

    /**
     * @throws Throw an error if the value is undefined.
     * @return fs.Stats Return the statistics informations of the instance.
     */
    getStatistics(): fs.Stats {
        if (!this.hasStatistics()){
            throw new Error("You cannot get the statistics informations because this value is undefined.");
        }

        return this.statistics!;
    }

    /**
     * @param parent Set to false if you don't want to remove the statistics value for the all the directory tree.
     * @description Invalidate the statistics informations.
     * @return this
     */
    removeStatistics(parent: boolean = true): this {
        if (this.hasDirectory() && parent){
            this.getDirectory().removeStatistics(parent);
        }

        this.statistics = null;

        return this;
    }

    /**
     * @param cache Set to the value to false if you want to force to load the statistics from the file or the directory.
     * @description Load the statistics of the file or the directory, and return it.
     * @return Promise<fs.Stats> Return the loaded statistics.
     */
    loadStatistics(cache: boolean = false): Promise<fs.Stats>{
        if (this.hasStatistics() && !cache){
            return Promise.resolve(this.getStatistics());
        }

        return new Promise<fs.Stats>((resolve,reject) => {
            fs.stat(this.getPath(),(err,stats) => {
                if(err){
                    reject(err);
                }else{
                    this.setIsExists(true);
                    this.setStatistics(stats);

                    resolve(this.getStatistics());
                }
            });
        });
    }

    /**
     * @param parent The new directory instance.
     * @description Set the directory instance. This function will not update our files.
     * @return this
     */
    setDirectory(parent: AbtractFolder){
        this.directory = parent;

        return this;
    }

    /**
     * @description Check if the instance have a directory instance or not.
     * @return boolean
     */
    hasDirectory(): boolean {
        return this.directory !== null;
    }

    /**
     * @throws Throw an error if the value of the directory is undefined.
     * @description Return the directory instance.
     * @return AbtractFolder
     */
    getDirectory(): AbtractFolder {
        if (!this.hasDirectory()){
            throw new Error("You cannot get the directory instance because this value is empty.");
        }

        return this.directory!;
    }
}

export default AbtractFolder;
