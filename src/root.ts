/**
 * @class Root
 * @description This class is the top level Path.
 */
class Root{
    /**
     * @description The basename path is the basename of the instance.
     */
    basename: null|string = null;

    /**
     * @description Set the basename of the instance. This method won't update our files or directory.
     * @param name The futur basenname of the instance.
     * @return this
     */
    setBasename(name: string): this {
        this.basename = name;

        return this;
    }

    /**
     * @description Check if the basename variable has a value or not.
     * @return boolean
     */
    hasBasename(): boolean {
        return this.basename !== null;
    }

    /**
     * @description Get the value of the basename of the current instance.
     * @throws Throw an error if the basename value does not exist.
     */
    getBasename(): string {
        if (!this.hasBasename()){
            throw new Error("You cannot get the basename value because this value is empty.");
        }

        return this.basename!;
    }
}


export default Root;