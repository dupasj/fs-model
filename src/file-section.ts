import FilePoint from "./file-point";
import File from "./file";

class FileSection{
    end: FilePoint|null = null;
    start: FilePoint|null = null;
    file: File|FileSection|null = null;

    trimStart(){
        const content = this.getContent();
        const left_trimed = content.trimLeft();
        const position = content.indexOf(left_trimed[0]);

        this.setStart(position + this.getStart().getPosition());

        return this;
    }

    trimEnd(){
        const content = this.getContent();
        const right_trimed = content.trimRight();
        const position = content.lastIndexOf(right_trimed.slice(-1));
        
        this.setEnd(position + this.getStart().getPosition() + 1);
        

        return this;
    }

    trim(){
        this.trimEnd();
        this.trimStart();

        return this;
    }

    setFile(file: File|FileSection){
        if (this.file !== file){
            this.file = file;

            if(this.hasEnd()){
                this.getEnd().setFile(file);
            }
            if(this.hasStart()){
                this.getStart().setFile(file);
            }
        }

        return this;
    }
    hasFile(){
        return this.file !== null;
    }
    getFile(){
        if (!this.hasFile()){
            throw new Error();
        }

        return this.file!;
    }

    setEnd(end: FilePoint|number): this {
        if (typeof end === "number"){
            if (this.hasEnd()){
                this.getEnd().setPosition(end);

                return this;
            }

            return this.setEnd(FilePoint.fromPosition(end));
        }

        this.end = end;

        end.mergeFileSection(this);

        return this;
    }
    hasEnd(){
        return this.end !== null;
    }
    getEnd(){
        if (!this.hasEnd()){
            throw new Error();
        }

        return this.end!;
    }

    setStart(start: FilePoint|number): this {
        if (typeof start === "number"){
            if (this.hasStart()){
                this.getStart().setPosition(start);

                return this;
            }

            return this.setStart(FilePoint.fromPosition(start));
        }

        this.start = start;

        start.mergeFileSection(this);

        return this;
    }
    hasStart(){
        return this.start !== null;
    }
    getStart(){
        if (!this.hasStart()){
            throw new Error();
        }

        return this.start!;
    }

    getContent(): string {
        return this.getFile().getContent().toString().slice(this.getStart().getPosition(),this.getEnd().getPosition());
    }

    static from(start: number,end: number,file?: File|FileSection){
        const file_section = new FileSection();
        file_section.setStart(start);
        file_section.setEnd(end);
        if (typeof file !== "undefined"){
            file_section.setFile(file);
        }

        return file_section;
    }
}

export default FileSection;
